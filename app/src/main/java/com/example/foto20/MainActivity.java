package com.example.foto20;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.gms.auth.api.signin.internal.Storage;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    ImageView mImageViewPhoto;
    Button mButtonTakePhoto;
    private StorageReference mStorage;

    String mAbsolutePath="";
    final int PHOTO_CONST =1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mImageViewPhoto = findViewById(R.id.imageViewPhoto);
        mButtonTakePhoto = findViewById(R.id.buttontakePhoto);

        mStorage= FirebaseStorage.getInstance().getReference();



        mButtonTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                abrirCamara();
            }
        });


        //insert


    }

    private void abrirCamara(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager())!=null){
            startActivityForResult(intent,1);
        }
    }

private File createFotoFile() throws IOException {
    String timestamp= new SimpleDateFormat("yyyyMMdd").format(new Date());
    String imageFileName="imagen"+timestamp;


    File storgeFile=getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    File photoFile=File.createTempFile(
            imageFileName,
            "jpg",
            storgeFile


    );
    mAbsolutePath=photoFile.getAbsolutePath();

    return photoFile;




}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PHOTO_CONST && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imagBitmap= (Bitmap) extras.get("data");
            Double a= Math.random();
            StorageReference mountainImagesRef= mStorage.child("imagenes/"+a+"usuario.png");
            ByteArrayOutputStream baos= new ByteArrayOutputStream();
            imagBitmap.compress(Bitmap.CompressFormat.PNG,100,baos);
            byte[] data2= baos.toByteArray();
            UploadTask uploadTask=mountainImagesRef.putBytes(data2);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                }
            });
           // mImageViewPhoto.setImageBitmap(imagBitmap);


          //  mImageViewPhoto.setImageURI(data.getData());

        }


    }
}

